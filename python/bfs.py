from binary_node import BinaryNode as Node

n1 = Node(value='A')
n2 = Node(value='B')
n3 = Node(value='C', left=n1, right=n2)
n4 = Node(value='D')
n5 = Node(value='E')
n6 = Node(value='F', left=n4, right=n5)
n7 = Node(value='G', left=n3, right=n6)

graph = n7

'''
Implement Breadth First Search
1. First, check if the root node is the target. If it is, return the root node.
2. Otherwise, add the children of the current node to the **queue** of nodes.
3. Pop each from the queue and check if each are the target.
4. Return if target. Otherwise, go to step 2 and repeat.
'''
def bfs(graph: Node, target_value: str) -> Node | None:
    queue = [graph]

    while len(queue) > 0:
        current = queue.pop(0)
        if current.value == target_value:
            return current

        left_node = current.left
        right_node = current.right

        if left_node:
            left_search = bfs(left_node, target_value)
            if left_search:
                return left_search
        if right_node:
            right_search = bfs(right_node, target_value)
            if right_search:
                return right_search

    return None


# Pass these tests
assert bfs(graph, 'G').value == 'G'
assert bfs(graph, 'C').left.value == 'A'
assert bfs(graph, 'F').right.value == 'E'
assert bfs(graph, 'E').left == None
