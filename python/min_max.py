from binary_node import BinaryNode as Node

n1 = Node(value=2)
n2 = Node(value=5)
n3 = Node(value=1, left=n1, right=n2)
n4 = Node(value=7)
n5 = Node(value=3)
n6 = Node(value=6, left=n4, right=n5)
n7 = Node(value=4, left=n3, right=n6)

graph = n7

'''
Traverse the tree to find the minimum value in the tree
'''
def min_of_tree(graph: Node) -> float | None:
    if not graph:
        return None

    min_value = float("Inf")
    stack = [graph]

    while len(stack) > 0:
        current = stack.pop()
        min_value = min(current.value, min_value)

        left_node = current.left
        right_node = current.right

        if left_node:
            stack.append(left_node)
        if right_node:
            stack.append(right_node)

    return min_value




'''
Traverse the tree to find the maximum value in the tree
'''
def max_of_tree(graph: Node) -> float | None:
    if not graph:
        return None

    max_value = float("-Inf")
    queue = [graph]

    while len(queue) > 0:
        current = queue.pop(0)
        max_value = max(current.value, max_value)

        left_node = current.left
        right_node = current.right

        if left_node:
            queue.append(left_node)
        if right_node:
            queue.append(right_node)

    return max_value

# Pass these tests
assert min_of_tree(graph) == 1
assert max_of_tree(graph) == 7
