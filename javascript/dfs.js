"use strict"

import BinaryNode from "./binaryNode.js";

const recursiveDFS = (graph, target) => {
    if (!graph) return null;
    if (graph.value === target) return graph;

    const left_node = graph.left;
    const left_search_result = recursiveDFS(left_node, target);
    if (left_search_result) return left_search_result;

    const right_node = graph.right;
    const right_search_result = recursiveDFS(right_node, target);
    if (right_search_result) return right_search_result;

    return null;
}


const iterativeDFS = (graph, target) => {
    if (!graph) return null;

    const stack = [graph];

    while (stack.length > 0) {
        const current = stack.pop();
        if (current.value === target) return current;

        const left_node = current.left;
        if (left_node) stack.push(left_node);

        const right_node = current.right;
        if (right_node) stack.push(right_node);
    }

    return null;
}


const n1 = new BinaryNode({value: 'A'})
const n2 = new BinaryNode({value: 'B'})
const n3 = new BinaryNode({value: 'C', left: n1, right: n2})
const n4 = new BinaryNode({value: 'D'})
const n5 = new BinaryNode({value: 'E'})
const n6 = new BinaryNode({value: 'F', left: n4, right: n5})
const n7 = new BinaryNode({value: 'G', left: n3, right: n6})


// TESTS: all logs should be true
const root = n7
console.log("recursive tests")
console.log(recursiveDFS(root, 'G').value === 'G')
console.log(recursiveDFS(root, 'C').left.value === 'A')
console.log(recursiveDFS(root, 'F').right.value === 'E')
console.log(recursiveDFS(root, 'E').left === null)

console.log("iterative tests")
console.log(iterativeDFS(root, 'G').value === 'G')
console.log(iterativeDFS(root, 'C').left.value === 'A')
console.log(iterativeDFS(root, 'F').right.value === 'E')
console.log(iterativeDFS(root, 'E').left === null)
