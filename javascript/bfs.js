"use strict"

import BinaryNode from './binaryNode.js';

/*
Implement Breadth First Search
1. First, check if the root node is the target. If it is, return the root node.
2. Otherwise, add the children of the current node to the **queue** of nodes.
3. Pop each from the queue and check if each are the target.
4. Return if target. Otherwise, go to step 2 and repeat.
*/
const bfs = (graph, target) => {
    const queue = [graph];

    while (queue.length > 0) {
        const current = queue.shift();
        if (current.value === target) return current;

        const left_node = current.left;
        const right_node = current.right;

        if (left_node) queue.push(left_node);
        if (right_node) queue.push(right_node);
    }

    return null;
}


const n1 = new BinaryNode({value: 'A'})
const n2 = new BinaryNode({value: 'B'})
const n3 = new BinaryNode({value: 'C', left: n1, right: n2})
const n4 = new BinaryNode({value: 'D'})
const n5 = new BinaryNode({value: 'E'})
const n6 = new BinaryNode({value: 'F', left: n4, right: n5})
const n7 = new BinaryNode({value: 'G', left: n3, right: n6})


// TESTS: all logs should be true
const root = n7
console.log(bfs(root, 'G').value === 'G')
console.log(bfs(root, 'C').left.value === 'A')
console.log(bfs(root, 'F').right.value === 'E')
console.log(bfs(root, 'E').left === null)
