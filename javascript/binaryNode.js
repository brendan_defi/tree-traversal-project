"use strict";

class BinaryNode {
    constructor({value = 0, left = null, right = null} = {}) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}

export default BinaryNode
