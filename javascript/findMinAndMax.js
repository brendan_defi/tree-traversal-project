"use strict"

import BinaryNode from "./binaryNode.js"


const treeMin = (graph) => {
    if (!graph) return;

    let minVal = Infinity;
    const queue = [graph];

    while (queue.length > 0) {
        const current = queue.shift();
        minVal = Math.min(current.value, minVal);

        const left_node = current.left;
        if (left_node) queue.push(left_node);

        const right_node = current.right;
        if (right_node) queue.push(right_node);
    }

    return minVal
}


const treeMaxIterative = (graph) => {
    if (!graph) return;

    let maxVal = -Infinity;
    const stack = [graph];

    while (stack.length > 0) {
        const current = stack.pop();
        maxVal = Math.max(current.value, maxVal);

        const left_node = current.left;
        if (left_node) stack.push(left_node);

        const right_node = current.right;
        if (right_node) stack.push(right_node);
    }

    return maxVal
}


const treeMaxRecursive = (graph, maxVal = -Infinity) => {
    if (!graph) return;

    maxVal = Math.max(graph.value, maxVal);

    const left_node = graph.left;
    const left_path_max = treeMaxRecursive(left_node, maxVal);
    if (left_path_max) maxVal = Math.max(maxVal, left_path_max);

    const right_node = graph.right;
    const right_path_max = treeMaxRecursive(right_node, maxVal);
    if (right_path_max) maxVal = Math.max(maxVal, right_path_max);

    return maxVal;
}


// TESTS: all logs should be true
const n1 = new BinaryNode({value: 2})
const n2 = new BinaryNode({value: 5})
const n3 = new BinaryNode({value: 1, left: n1, right: n2})
const n4 = new BinaryNode({value: 7})
const n5 = new BinaryNode({value: 3})
const n6 = new BinaryNode({value: 6, left: n4, right: n5})
const n7 = new BinaryNode({value: 4, left: n3, right: n6})


// TESTS: all logs should be true
const root = n7
console.log(treeMin(root) === 1)
console.log(treeMaxIterative(root) === 7)
console.log(treeMaxRecursive(root) === 7)
